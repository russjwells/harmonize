﻿#pragma strict

var ageDays:int;
var appleTimer:int;
var appleMax:int;
var appleGrowDays:int;
var hasApples: boolean;
var youngTreeLifeLength:int;
var growthRate:int;

var FruitMount1: GameObject;
var FruitMount2: GameObject;
var FruitMount3: GameObject;
var myFruit: GameObject;

//var isSelected: boolean = false;

function Start () {
	InvokeRepeating("nextDay", 0, 3);
	//ageDays=0;
	appleGrowDays=10;
	appleMax=3;
	GameObject.Find("GameController").SendMessage("addPoints", 25);
}

function nextDay(){
	grow();
}

function grow(){
	//add days
	//
		ageDays++;
	
	
	//growth actions
	//
		appleTimer++;
		
		
	//produce
	//
	if (!hasApples){
		if (appleTimer>=appleGrowDays){
			growApples();
			appleTimer=0;
		
		}
	}
}

function Update () {

	if (hasApples==true){
		if (ageDays>360){
			dropApples();
		}
	}

}
function dropApples(){
	Debug.Log("Drop apples");
	for (var child : Transform in transform) {
    		Debug.Log("childs: "+child);
    		if (child.GetComponent(apple)){
				//child.GetComponent(Rigidbody).useGravity=true;
				//child.GetComponent(Rigidbody).isKinematic=false;
				child.SendMessage("drop");
			}
    	}
}

function growApples(){
		var pos;
		pos=FruitMount1.transform.position;
		var myFruit1 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit1.transform.parent=transform;
		myFruit1.GetComponent(apple).growthRate=growthRate;
		pos=FruitMount2.transform.position;
		var myFruit2 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit2.transform.parent=transform;
		myFruit2.GetComponent(apple).growthRate=growthRate;
		pos=FruitMount3.transform.position;
		var myFruit3 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit3.transform.parent=transform;
		myFruit3.GetComponent(apple).growthRate=growthRate;
		hasApples=true;
		//growthRate=growthRate;
		Debug.Log("big tree grew apples");

}

function harvest(){
	for (var child : Transform in transform) {
    		if (child.GetComponent(apple)){
    			Debug.Log("picked "+child);
				child.GetComponent(Rigidbody).useGravity=true;
				child.GetComponent(Rigidbody).isKinematic=false;
				child.SendMessage("pick");
			}
    	}
    	hasApples=false;
}

function mulch(){
	Debug.Log("mulch");
	var pos;
	pos = transform.position;
	for (var m = 0;m<=growthRate;m++){
		Instantiate(Resources.Load("mulchBit", GameObject), pos, transform.rotation);
		Debug.Log("mulch "+m);
	}
	Destroy(this.gameObject);
}

function knock(){
	dropApples();
}

function dayAge(d:int){
	ageDays = ageDays+d;
	Debug.Log("Tree grew");
}