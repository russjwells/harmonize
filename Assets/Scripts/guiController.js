﻿#pragma strict

//var guiModeGlobal             : String;
var tryingToPlace             : boolean;
var placeProjectorHeight      : int;
var isSeedsMenu               : boolean;
var isInfoPane				  : boolean;

var SeedsMenu : GameObject;
Debug.Log("guiController()");

function Start () {
	isSeedsMenu=false;
	isInfoPane=false;
}

function Update () {
	var points:int;
	points = GameObject.Find("GameController").GetComponent(gameController).points;
	GameObject.Find("Points");

	//fullscreen toggle
	//
    if (Input.GetKeyDown(KeyCode.F)){
    	Screen.fullScreen = !Screen.fullScreen;
    	Debug.Log("full screen toggle");
    }

    //seed menu toggle 
    //space button
    //
    if (Input.GetKeyDown(KeyCode.Space)){
    	if (isSeedsMenu==false){
    		hideTabMenu();
	    	Debug.Log("Show seeds menu");
	    	showSeedsMenu();
    	}else{
	   	 	Debug.Log("Hide seeds menu");
	    	hideSeedsMenu();
    	}
    }
    
    
    //InfoPanel
    if (Input.GetKeyDown(KeyCode.Tab)){
    	if (isInfoPane==false){
	    	Debug.Log("Show info menu");
	    	showTabMenu();
    	}else{
	    	isInfoPane=false;
	    	hideTabMenu();
    	}
    }
    
    //Escape
    if (Input.GetKeyDown(KeyCode.Escape)){
    	//back to main menu
    	Application.LoadLevel("TitleScreen");
    }
    
    //place plants
	//
	
	//PLACE SNAP TO POINTER
	if (tryingToPlace==true){
		//Debug.Log("Place Projector Snap");
		var ray=Camera.main.ScreenPointToRay (Input.mousePosition);
		var hit:RaycastHit;
		if (Physics.Raycast(ray,hit)){
			//Debug.Log("I am looking at" + hit.transform.name + " @ " + hit.point);
			GameObject.Find("placeProjector").GetComponent(Projector).enabled = true;
			GameObject.Find("placeProjector").transform.position.x = hit.point.x;
			GameObject.Find("placeProjector").transform.position.z = hit.point.z;
			GameObject.Find("placeProjector").transform.position.y = hit.point.y+placeProjectorHeight;
			GameObject.Find("GameController").SendMessage("syncTryingToPlace",true);
		}else{
//			Debug.Log("no good raycast");
		}
	}
	
	
}

//placing plants
//
function syncTryingToPlace(placing:boolean){
	tryingToPlace=placing;
//	Debug.Log("TryingToPlace is");
	//Debug.Log(tryingToPlace);
}

function hideSeedsMenu(){
			isSeedsMenu=false;
			GameObject.Find("SeedsMenu").guiTexture.enabled=false;
    		GameObject.Find("AppleSeedsLabel").guiText.enabled=false;
    		GameObject.Find("GoggiBerryBushesLabel").guiText.enabled=false;
    		GameObject.Find("SeedsMenuBackground").guiTexture.enabled=false;
    		GameObject.Find("SeedsTab").guiTexture.enabled=false;
    		GameObject.Find("SeedsTabTitle").guiText.enabled=false;
    		GameObject.Find("appletreeimg").guiTexture.enabled=false;
    		GameObject.Find("goggiimg").guiTexture.enabled=false;
    		GameObject.Find("spaceToOpen").guiText.enabled=true;
    		GameObject.Find("spaceToClose").guiText.enabled=false;
    		GameObject.Find("MorePlants").guiText.enabled=false;
}

function showSeedsMenu(){
			isSeedsMenu=true;
	    	GameObject.Find("SeedsMenu").guiTexture.enabled=true;
	    	GameObject.Find("AppleSeedsLabel").guiText.enabled=true;
	    	GameObject.Find("GoggiBerryBushesLabel").guiText.enabled=true;
	    	GameObject.Find("SeedsMenuBackground").guiTexture.enabled=true;
	    	GameObject.Find("SeedsTab").guiTexture.enabled=true;
	    	GameObject.Find("SeedsTabTitle").guiText.enabled=true;
	    	GameObject.Find("appletreeimg").guiTexture.enabled=true;
	    	GameObject.Find("goggiimg").guiTexture.enabled=true;
	    	GameObject.Find("spaceToOpen").guiText.enabled=false;
	    	GameObject.Find("spaceToClose").guiText.enabled=true;
	    	GameObject.Find("MorePlants").guiText.enabled=true;
}

function hideTabMenu(){
			isInfoPane=false;
			GameObject.Find("InfoTab").guiTexture.enabled=false;
	    	GameObject.Find("InfoClose").guiText.enabled=false;
	    	GameObject.Find("InfoLabelTab").guiTexture.enabled=false;
	    	GameObject.Find("InfoTabTitle").guiText.enabled=false;
	    	GameObject.Find("InfoOutline").guiTexture.enabled=false;
	    	GameObject.Find("menu_hardsep").guiTexture.enabled=false;
	    	GameObject.Find("menu_softsep1").guiTexture.enabled=false;
	    	//GameObject.Find("menu_softsep2").guiTexture.enabled=false;
	    	GameObject.Find("selectionImage").guiTexture.enabled=false;
	    	
	    	GameObject.Find("Stats").guiText.enabled=false;
	    	GameObject.Find("Age").guiText.enabled=false;
	    	GameObject.Find("AgeNum").guiText.enabled=false;
	    	GameObject.Find("Fruit").guiText.enabled=false;
	    	GameObject.Find("FruitNum").guiText.enabled=false;
	    	GameObject.Find("Growth Rate").guiText.enabled=false;
			GameObject.Find("GRNum").guiText.enabled=false;
			
//			GameObject.Find("Effects").guiText.enabled=false;
	//		GameObject.Find("Soil Nutrients").guiText.enabled=false;
	//		GameObject.Find("NutrientsNum").guiText.enabled=false;
//			GameObject.Find("Shade").guiText.enabled=false;
	//		GameObject.Find("ShadeNum").guiText.enabled=false;
			
			GameObject.Find("Actions").guiText.enabled=false;
			GameObject.Find("Mulch").guiText.enabled=false;
			GameObject.Find("PickFruit").guiText.enabled=false;
			GameObject.Find("Knock").guiText.enabled=false;
			
			GameObject.Find("Description").guiText.enabled=false;
			
}

function showTabMenu(){
			//Debug.Log("Show info menu");
	    	isInfoPane=true;
	    	GameObject.Find("InfoTab").guiTexture.enabled=true;
	    	GameObject.Find("InfoClose").guiText.enabled=true;
	    	GameObject.Find("InfoLabelTab").guiTexture.enabled=true;
	    	GameObject.Find("InfoTabTitle").guiText.enabled=true;
	    	GameObject.Find("InfoOutline").guiTexture.enabled=true;
	    	GameObject.Find("menu_hardsep").guiTexture.enabled=true;
	    	GameObject.Find("menu_softsep1").guiTexture.enabled=true;
	    	//GameObject.Find("menu_softsep2").guiTexture.enabled=true;
	    	GameObject.Find("selectionImage").guiTexture.enabled=true;
	    	
	    	GameObject.Find("Stats").guiText.enabled=true;
	    	GameObject.Find("Age").guiText.enabled=true;
	    	GameObject.Find("AgeNum").guiText.enabled=true;
	    	GameObject.Find("Fruit").guiText.enabled=true;
	    	GameObject.Find("FruitNum").guiText.enabled=true;
	    	GameObject.Find("Growth Rate").guiText.enabled=true;
	    	GameObject.Find("GRNum").guiText.enabled=true;
	    	
//	    	GameObject.Find("Effects").guiText.enabled=true;
//	    	GameObject.Find("Soil Nutrients").guiText.enabled=true;
//			GameObject.Find("NutrientsNum").guiText.enabled=true;
//			GameObject.Find("Shade").guiText.enabled=true;
//			GameObject.Find("ShadeNum").guiText.enabled=true;
	    	
	   		GameObject.Find("Actions").guiText.enabled=true;
	    	GameObject.Find("Mulch").guiText.enabled=true;
			GameObject.Find("PickFruit").guiText.enabled=true;
			GameObject.Find("Knock").guiText.enabled=true;
			
			GameObject.Find("Description").guiText.enabled=true;

}



