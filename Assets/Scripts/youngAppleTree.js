﻿#pragma strict

var ageDays:int;
var appleTimer:int;
var appleMax:int;
var appleGrowDays:int;
var hasApples: boolean;
var youngTreeLifeLength:int;
var growthRate:int;

var FruitMount1: GameObject;
var FruitMount2: GameObject;
var FruitMount3: GameObject;
var myFruit: GameObject;
//var apples[]: GameObject;
function Start () {
	InvokeRepeating("nextDay", 0, 3);
	appleGrowDays=5;
	appleMax=5;
	hasApples=false;
	GameObject.Find("GameController").SendMessage("addPoints", 10);
}

function Update () {

}

function nextDay(){
	grow();
}

function grow(){
	//add days
	//
		ageDays++;
		
	//growth actions
	//
		appleTimer++;
		
	//evolve
	if (ageDays >20){
		makeAppleTree();
	}else{
		
	}
	//produce
	//if the tree has no apples
	if (!hasApples){
		if (appleTimer>=appleGrowDays){
			for(var i=0;i<=appleMax;i++){
					//Instantiate(Resources.Load("apple", GameObject), transform.position, transform.rotation);
				}
		//or
		//GameObject.Find(AppleMount1).transform;
		var pos;
		pos=FruitMount1.transform.position;
		var myFruit1 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit1.transform.parent=transform;
		myFruit1.GetComponent(apple).growthRate=growthRate;
		pos=FruitMount2.transform.position;
		var myFruit2 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit2.transform.parent=transform;
		myFruit2.GetComponent(apple).growthRate=growthRate;
		pos=FruitMount3.transform.position;
		var myFruit3 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit3.transform.parent=transform;
		myFruit3.GetComponent(apple).growthRate=growthRate;
				
			
		appleTimer=0;
		hasApples=true;
		}
	}
}

function harvest(){
	for (var child : Transform in transform) {
    		if (child.GetComponent(apple)){
    			Debug.Log("picked "+child);
				//child.GetComponent(Rigidbody).useGravity=true;
				//child.GetComponent(Rigidbody).isKinematic=false;
				child.SendMessage("pick");
			}
    	}
    	hasApples=false;
}
function mulch(){
	Debug.Log("mulch");
	var pos;
	pos = transform.position;
	for (var m = 0;m<=growthRate;m++){
		Instantiate(Resources.Load("mulchBit", GameObject), pos, transform.rotation);
		Debug.Log("mulch "+m);
	}
	Destroy(this.gameObject);
}

function knock(){
	dropApples();
}

function dropApples(){
	Debug.Log("Drop apples");
	for (var child : Transform in transform) {
    		Debug.Log("childs: "+child);
    		if (child.GetComponent(apple)){
				//child.GetComponent(Rigidbody).useGravity=true;
				//child.GetComponent(Rigidbody).isKinematic=false;
				child.SendMessage("drop");
			}
    	}
}

function makeAppleTree(){
	//create
	var newAppleTree : GameObject = Instantiate(Resources.Load("appleTree", GameObject));
	
	//position
	newAppleTree.transform.position=transform.position;
	newAppleTree.transform.position.y=transform.position.y-9;
	
	//pass Variables
	newAppleTree.GetComponent(appleTree).ageDays=ageDays;
	//newAppleTree.GetComponent(appleTree).hasApples=hasApples;
	newAppleTree.GetComponent(appleTree).appleGrowDays=appleGrowDays;
	
	newAppleTree.GetComponent(appleTree).ageDays=ageDays;
	newAppleTree.GetComponent(appleTree).growthRate=growthRate;
	
	//rotation
	newAppleTree.transform.rotation=transform.rotation;
	
	//transfer selection
	if (gameObject.GetComponent(selectable).isSelected==true){
		GameObject.Find("GameController").SendMessage("select", newAppleTree);
	}
	
	
	//Debug.Log("I'm growing into a big apple tree!");
	Destroy(gameObject);
}