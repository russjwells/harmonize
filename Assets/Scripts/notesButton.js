﻿#pragma strict

var notesButtonOff : Texture2D;
var notesButtonSelected : Texture2D;
var notesButtonHover : Texture2D;
var isSelected : boolean = false;

function Start () {

}

function OnMouseEnter () {

	Debug.Log("Mouse Enter");
	if (isSelected == false){
		guiTexture.texture = notesButtonHover;
	}
}

function OnMouseExit () {

	Debug.Log("Mouse Exit");
	if (isSelected == false){
		guiTexture.texture = notesButtonOff;
	}

}

function OnMouseDown () {

	Debug.Log("Mouse Down");
	if (isSelected == false){
		isSelected = true;
		guiTexture.texture = notesButtonSelected;
		SendMessageUpwards("setGuiMode","Notes");
	}else{
		isSelected = false;
		guiTexture.texture = notesButtonOff;
	}

}

function switchOff(){
	isSelected=false;
	guiTexture.texture = notesButtonOff;
}