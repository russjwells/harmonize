﻿#pragma strict

var plantsButtonOff : Texture2D;
var plantsButtonSelected : Texture2D;
var plantsButtonHover : Texture2D;
var isSelected : boolean = false;

function Start () {

}

function OnMouseEnter () {

	//Debug.Log("Mouse Enter");
	if (isSelected == false){
		guiTexture.texture = plantsButtonHover;
	}

}

function OnMouseExit () {

	//Debug.Log("Mouse Exit");
	if (isSelected == false){
		guiTexture.texture = plantsButtonOff;
	}

}

function OnMouseDown () {

	//Debug.Log("Mouse Down");
	if (isSelected == false){
		isSelected = true;
		guiTexture.texture = plantsButtonSelected;
		//GameObject.Find("guiController").
		SendMessageUpwards("setGuiMode","Plants");
	}else{
		isSelected = false;
		guiTexture.texture = plantsButtonOff;
	}		
}


function switchOff(){
	isSelected=false;
	guiTexture.texture = plantsButtonOff;
}