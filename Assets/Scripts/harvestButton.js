﻿#pragma strict

var harvestButtonOff : Texture2D;
var harvestButtonSelected : Texture2D;
var harvestButtonHover : Texture2D;
var isSelected : boolean = false;

function Start () {

}

function OnMouseEnter () {

	Debug.Log("Mouse Enter");
	if (isSelected == false){
		guiTexture.texture = harvestButtonHover;
	}
}

function OnMouseExit () {

	Debug.Log("Mouse Exit");
	if (isSelected == false){
		guiTexture.texture = harvestButtonOff;
	}

}

function OnMouseDown () {

	Debug.Log("Mouse Down");
	if (isSelected == false){
		isSelected = true;
		guiTexture.texture = harvestButtonSelected;
		SendMessageUpwards("setGuiMode","Harvest");
	}else{
		isSelected = false;
		guiTexture.texture = harvestButtonOff;
	}

}

function switchOff(){
	isSelected=false;
	guiTexture.texture = harvestButtonOff;
}