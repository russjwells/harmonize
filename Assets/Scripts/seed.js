﻿#pragma strict

var ageDays:int;
var growthRate:int;

function Start () {
	//GetComponent(MeshCollider).enabled=false;
	InvokeRepeating("nextDay", 0, 3);
	ageDays=0;
	growthRate=Random.Range(1, 101);
}

function nextDay(){
	//ageDays++;
	grow();
}
function Update () {

}

function grow(){
	ageDays = ageDays + 1;
	
	if (ageDays>1){
	
	}
	
	if (ageDays >4){
		makeTree();
	}else{
		
	}
}

function makeTree(){
	//create the little tree
	var newYoungAppleTree : GameObject = Instantiate(Resources.Load("youngAppleTree", GameObject));
	//place the tree
	newYoungAppleTree.transform.position=transform.position;
	newYoungAppleTree.transform.position.y=transform.position.y+8;
	//random rotation
	var randomRotation = Quaternion.Euler( 0 , Random.Range(0, 360) , 0);
	newYoungAppleTree.transform.rotation=randomRotation;
	//transfer age and growth rate
	newYoungAppleTree.GetComponent(youngAppleTree).ageDays=ageDays;
	newYoungAppleTree.GetComponent(youngAppleTree).growthRate=growthRate;
	//transfer selection
	if (gameObject.GetComponent(selectable).isSelected==true){
		GameObject.Find("GameController").SendMessage("select", newYoungAppleTree);
	}
	//update info
	//destroy seed
	Destroy(gameObject);
	
}
function knock(){
  	rigidbody.AddForce(Vector3(10,10,10));
 	 Debug.Log("Kick Seed");
}