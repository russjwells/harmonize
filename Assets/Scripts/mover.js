﻿#pragma strict

var walkspeed: float = 5.0;

    // Attach to a camera
     
    var mDelta = 10; // Pixels. The width border at the edge in which the movement work
    var mSpeed = 25.0; // Scale. Speed of the movement
     
    private var mRightDirection = Vector3.right; // Direction the camera should move when on the right edge
    private var mLeftDirection = Vector3.left; // Direction the camera should move when on the right edge
    private var mForwardDirection = Vector3.forward;
    private var mBackDirection = Vector3.back;

function Start() {



}

function Update() {
	//right edge
	if ( Input.mousePosition.x >= Screen.width - mDelta )
        {
            // Move the camera
            transform.position += mRightDirection * Time.deltaTime * mSpeed;
        }
        
    //left edge
    
    	if ( Input.mousePosition.x<= 0 + mDelta )
        {
            // Move the camera
            transform.position += mLeftDirection * Time.deltaTime * mSpeed;
        }
    
    //top
    
    if ( Input.mousePosition.y<= 0 + mDelta )
        {
            // Move the camera
            transform.position += mBackDirection * Time.deltaTime * mSpeed;
        }
    
    //bottom
    
        if ( Input.mousePosition.y>= Screen.height - mDelta )
        {
            // Move the camera
            transform.position += mForwardDirection * Time.deltaTime * mSpeed;
        }

    rigidbody.freezeRotation = true;

    if (Input.GetKey("w")) transform.Translate(Vector3(0, 0, 1) * Time.deltaTime * walkspeed);
    if (Input.GetKey("s")) transform.Translate(Vector3(0, 0, - 1) * Time.deltaTime * walkspeed);
    if (Input.GetKey("a")) transform.Translate(Vector3(-1, 0, 0) * Time.deltaTime * walkspeed);
    if (Input.GetKey("d")) transform.Translate(Vector3(1, 0, 0) * Time.deltaTime * walkspeed);

}

