﻿#pragma strict

var points:int;

var tryingToPlace: boolean;
var newPlacing : GameObject;
var selected : GameObject;
var myPlants : ArrayList;
var diamondHeight : int;
//var myPlants = new ArrayList();

var name1:String;
var age;
var fruit;
var GRNum;

var appleTreePic:Texture2D;
var goggiBerryBushPic:Texture2D;

function Start () {
	points=0;
	//hide tab menu
	GameObject.Find("GUI Holder").SendMessage("hideSeedsMenu");
	GameObject.Find("GUI Holder").SendMessage("hideTabMenu");
	
	deSelect();
}




function countPlants():int{

	return myPlants.Count;
}

function findClosestPlant(plant_x : int, plant_y : int) : GameObject {

    var closestPlant         : GameObject;
    var shortestDistance    : float = 9999;
    var plantPos             : Vector3;
    var coordinatePos       : Vector3;
    var distanceVector      : Vector3;

    for(var plant : GameObject in myPlants){
        plantPos = plant.transform.position;
        //coordinatePos = Vector3(plant_x, gameController.y_height, plant_y);
        distanceVector = plantPos - coordinatePos;

       
        if (distanceVector.magnitude < shortestDistance){
            closestPlant = plant;
            shortestDistance = distanceVector.magnitude;
        }
    }
    return closestPlant;
}

function Update () {
//


if (selected){
	updateInfo(selected);
	//keep diamond over moving selected objects
	GameObject.Find("selectionDiamond").transform.position.x=selected.transform.position.x;
	GameObject.Find("selectionDiamond").transform.position.y=selected.transform.position.y+diamondHeight;
	GameObject.Find("selectionDiamond").transform.position.z=selected.transform.position.z;
	
	
	
	if(Input.GetKeyDown("p")){
		Debug.Log("p pressed on selected");
		selected.SendMessage("harvest");
		}
	}
	if(Input.GetKeyDown("m")){
		Debug.Log("m pressed on selected");
		selected.SendMessage("mulch");
		//Destroy(selected);
		deSelect();
		GameObject.Find("GUI Holder").SendMessage("hideTabMenu");
	}
	if(Input.GetKeyDown("k")){
		Debug.Log("k pressed on selected");
			selected.SendMessage("knock");
	}

if (tryingToPlace){
	//Debug.Log("game controller trying to place");
	//circle of light
	//newPlacing.transform.position.x=GameObject.Find("placeProjector").transform.position.x;
	//newPlacing.transform.position.z=GameObject.Find("placeProjector").transform.position.z;
	
	//may not need to raycast twice, just doing this real quick 
	//to make sure the newPlacing sits on top of the terrain
	var ray=Camera.main.ScreenPointToRay (Input.mousePosition);
	var hit:RaycastHit;
	if (Physics.Raycast(ray,hit)){
		newPlacing.transform.position.y = hit.point.y+5;
		newPlacing.transform.position.x = hit.point.x;
		newPlacing.transform.position.z = hit.point.z;
	}else{
		Debug.Log("weird object/terrain placing error");
	}
	
	//place
	//click to place what is trying to be placed
	if(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(1)){
	//Input.GetMouseButtonDown(0)
		//add test to find if there is room
		Debug.Log("let go of placing");
		tryingToPlace=false;
		GameObject.Find("GUI Holder").SendMessage("syncTryingToPlace", false);
		newPlacing.GetComponent(MeshCollider).enabled=true;
		GameObject.Find("placeProjector").GetComponent(Projector).enabled = false;
		//newPlacing.tag="Plant";
		//myPlants.Add(newPlacing);
	}else{
		//can't be placed here
	}
}


//if left click
if(Input.GetMouseButtonDown(0)){
		//find what is clicked on
		var ray2=Camera.main.ScreenPointToRay (Input.mousePosition);
		var hit2:RaycastHit;
		if (Physics.Raycast(ray2,hit2)){
			//Debug.Log(hit2.transform.gameObject);
			//Debug.Log("Clicked on "+hit2.transform.gameObject.name);
			//getObject from hit
			var clickedObject = hit2.transform.gameObject;
			Debug.Log(clickedObject);
			//if object has a parent, select the parent
			if (clickedObject.transform.parent){
				Debug.Log("Select Parent");
				clickedObject=clickedObject.transform.parent.gameObject;
				Debug.Log(clickedObject);
			}else{
			
			}
			Debug.Log(clickedObject);
			//if object has a parent, select the parent
			if (clickedObject.transform.parent){
				Debug.Log("Select Parent again");
				clickedObject=clickedObject.transform.parent.gameObject;
				Debug.Log(clickedObject);
			}else{
			
			}

			//if it's selectable
			if (clickedObject.GetComponent(selectable)){
			Debug.Log("it's selectable");
				//deselect current selection
				if (selected){
					selected.GetComponent(selectable).isSelected=false;
					deSelect();
				}
				//select it
				select(clickedObject);
				
				GameObject.Find("GUI Holder").SendMessage("hideSeedsMenu");
				GameObject.Find("GUI Holder").SendMessage("showTabMenu");
				//update info
				updateInfo(selected);
				//show info
				GameObject.Find("GUI Holder").SendMessage("showTabMenu");
			}
			
			if (clickedObject=="Terrain"){
				deSelect();
				Debug.Log("terrainnnnnns");
			}
			
			
		}
		}else{
			//no left click
			
			//selected.GetComponent(selectable).isSelected=false;
//			Debug.Log("unselect");
		}
}

function updateInfo(aboutObject:GameObject){
	if (aboutObject.GetComponent(selectable).plantType=="Apple Seed"){
		//Debug.Log("apple seed age display");
		//name1=selected.GetComponent(seed).name;
		GameObject.Find("InfoTabTitle").guiText.text="Apple Seed";
		age=aboutObject.GetComponent(seed).ageDays.ToString();
		GameObject.Find("AgeNum").guiText.text=age;
		GRNum=aboutObject.GetComponent(seed).growthRate.ToString();
		GameObject.Find("GRNum").guiText.text=GRNum;
		
		GameObject.Find("FruitNum").guiText.text="No";
		//GameObject.Find("InfoTabTitle").guiText.text=selected.GetComponent(selectable).gameObject.name;
		GameObject.Find("Description").guiText.text="This seed will grow into a young apple tree.";
		GameObject.Find("selectionImage").guiTexture.texture=appleTreePic;
	}
	if (aboutObject.GetComponent(selectable).plantType=="Young Apple Tree"){
		GameObject.Find("InfoTabTitle").guiText.text="Young Apple Tree";
		age=aboutObject.GetComponent(youngAppleTree).ageDays.ToString();
		GameObject.Find("AgeNum").guiText.text=age;
		GRNum=aboutObject.GetComponent(youngAppleTree).growthRate.ToString();
		GameObject.Find("GRNum").guiText.text=GRNum;
		
		if (aboutObject.GetComponent(youngAppleTree).hasApples){
			GameObject.Find("FruitNum").guiText.text="Yes";
		}else{
			GameObject.Find("FruitNum").guiText.text="No";
		};
		GameObject.Find("Description").guiText.text="If a young tree is low in vitality, then maybe \nit's better off as mulch...";
		GameObject.Find("selectionImage").guiTexture.texture=appleTreePic;
		//GameObject.Find("Agenum").guiText.text=selected.GetComponent(youngAppleTree).ageDays.ToString();
		//GameObject.Find("InfoTabTitle").guiText.text=selected.GetComponent(youngAppleTree).gameObject.name;
	}
	if (aboutObject.GetComponent(selectable).plantType=="Apple Tree"){
		GameObject.Find("InfoTabTitle").guiText.text="Apple Tree";
		age=aboutObject.GetComponent(appleTree).ageDays.ToString();
		GameObject.Find("AgeNum").guiText.text=age;
		GRNum=aboutObject.GetComponent(appleTree).growthRate.ToString();
		GameObject.Find("GRNum").guiText.text=GRNum;
		
		if (aboutObject.GetComponent(appleTree).hasApples){
			GameObject.Find("FruitNum").guiText.text="Yes";
		}else{
			GameObject.Find("FruitNum").guiText.text="No";
		};
		GameObject.Find("Description").guiText.text="Apple trees are a staple in any food forest.";
		GameObject.Find("selectionImage").guiTexture.texture=appleTreePic;
	}
	
	if (aboutObject.GetComponent(selectable).plantType=="GoggiBerryBushes"){
		GameObject.Find("InfoTabTitle").guiText.text="Goggi Berry Bushes";
		age=aboutObject.GetComponent(goggiBerryBushes).ageDays.ToString();
		GameObject.Find("AgeNum").guiText.text=age;
		GRNum=aboutObject.GetComponent(goggiBerryBushes).growthRate.ToString();
		GameObject.Find("GRNum").guiText.text=GRNum;
		
		if (aboutObject.GetComponent(goggiBerryBushes).hasFruit){
			GameObject.Find("FruitNum").guiText.text="Yes";
		}else{
			GameObject.Find("FruitNum").guiText.text="No";
		};
		GameObject.Find("Description").guiText.text="Goggi roots support nitrogen-fixing bacteria. \nPlants near them will produce better fruit.";
		GameObject.Find("selectionImage").guiTexture.texture=goggiBerryBushPic;
	}
	
	if (aboutObject.GetComponent(selectable).plantType=="Goggi Seed"){
		GameObject.Find("InfoTabTitle").guiText.text="Goggi Berry Seed";
		age=aboutObject.GetComponent(goggiSeed).ageDays.ToString();
		GameObject.Find("AgeNum").guiText.text=age;
		GRNum=aboutObject.GetComponent(goggiSeed).growthRate.ToString();
		GameObject.Find("GRNum").guiText.text=GRNum;
		
		GameObject.Find("FruitNum").guiText.text="No";
		GameObject.Find("Description").guiText.text="Soon this seed will grow into bush with \nmany tasty berries.";
		GameObject.Find("selectionImage").guiTexture.texture=goggiBerryBushPic;
	}
}

function syncTryingToPlace(placing:boolean){
	tryingToPlace=placing;
//	Debug.Log("Sync");
//	Debug.Log("TryingToPlace!!!!!!!!!!!!!!");
//	Debug.Log(tryingToPlace);
}

function spawnPlant(plantType:String){
	//var objectToSpawn : GameObject;
	//var myTree : GameObject;
	//objectToSpawn = myTree;
	var newPlant : GameObject = Instantiate(Resources.Load(plantType, GameObject), GameObject.Find("InFrontOfCamera").transform.position, transform.rotation);
	//
	Debug.Log("Creating a new plant: " + plantType);
	tryingToPlace=true;
	newPlacing = newPlant;
	newPlacing.GetComponent(MeshCollider).enabled=true;
	//Instantiate(objectToSpawn, new Vector3(0, 0, 0), Quaternion.identity);
	
}
function select(newSelection : GameObject){

	GameObject.Find("selectionDiamond").GetComponentInChildren(MeshRenderer).renderer.enabled=true;
	//move diamond
	GameObject.Find("selectionDiamond").transform.position.x=newSelection.transform.position.x;
	GameObject.Find("selectionDiamond").transform.position.y=newSelection.transform.position.y+diamondHeight;
	GameObject.Find("selectionDiamond").transform.position.z=newSelection.transform.position.z;
	
	Debug.Log("New selection: " +newSelection+" !!!");
	
	//make it so
	newSelection.GetComponent(selectable).isSelected=true;
	selected=newSelection;
	
	//update info
}

function deSelect(){

	if(selected!=null){
		selected.GetComponent(selectable).isSelected=false;
		selected=null;
		Debug.Log("Deselected!!!");
		GameObject.Find("selectionDiamond").GetComponentInChildren(MeshRenderer).renderer.enabled=false;
		Debug.Log("Deselected");
	}else{
		Debug.Log("Nothing was selected");
	}
	
}

function addPoints(howMany:int){

	points = points + howMany;
	Debug.Log(howMany +" points added");

}