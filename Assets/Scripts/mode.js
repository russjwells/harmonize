﻿#pragma strict

var fadeDuration = 3.0;

function Start () {

    //yield Fade (0.0, 1.0, fadeDuration);

    yield Fade (1.0, 0.0, fadeDuration);

    Destroy(gameObject);
}


function Fade (startLevel, endLevel, time) {

    var speed = 1.0/9;///time;

   

    for (var t = 0.0; t < 1.0; t += Time.deltaTime*speed) {

        guiText.material.color.a = Mathf.Lerp(startLevel, endLevel, t);

        yield;

    }

}


function Update () {

//this.alpha--;
}