﻿#pragma strict

var soilButtonOff : Texture2D;
var soilButtonSelected : Texture2D;
var soilButtonHover : Texture2D;
var isSelected : boolean = false;

function Start () {

}

function OnMouseEnter () {

	//Debug.Log("Mouse Enter");
	if (isSelected == false){
		guiTexture.texture = soilButtonHover;
	}
}

function OnMouseExit () {

	//Debug.Log("Mouse Exit");
	if (isSelected == false){
		guiTexture.texture = soilButtonOff;
	}
}

function OnMouseDown () {

	//Debug.Log("Mouse Down");
	if (isSelected == false){
		isSelected = true;
		guiTexture.texture = soilButtonSelected;
		SendMessageUpwards("setGuiMode","Soil");
	}else{
		isSelected = false;
		guiTexture.texture = soilButtonOff;
	}

}
function switchOff(){
	isSelected=false;
	guiTexture.texture = soilButtonOff;
}