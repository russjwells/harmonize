﻿#pragma strict
var fadeDuration = 3.0;
var points:int;

function Start () {
	yield Fade (0.0, 1.0, fadeDuration);
}

function Update () {

	points=GameObject.Find("GameController").GetComponent(gameController).points;
	gameObject.guiText.text=points.ToString();

}



function Fade (startLevel, endLevel, time) {

    var speed = 1.0/3;///time;

   

    for (var t = 0.0; t < 1.0; t += Time.deltaTime*speed) {

        guiText.material.color.a = Mathf.Lerp(startLevel, endLevel, t);

        yield;

    }

}
