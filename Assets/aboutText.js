﻿#pragma strict

function Start () {
	guiText.text="harmonize is a game about permaculture gardening. \n It's meant to promote the use of permanent, local\n polyculture gardens. Today, most food is grown in large\n monoculture crops that are destructive to the environment \n and require inefficient use of our resources.";
}

function Update () {

}