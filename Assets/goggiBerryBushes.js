#pragma strict
var ageDays:int;
var hasFruit:boolean;
var fruitMax:int=15;

var myFruit: GameObject;
var FruitMount1:GameObject;
var FruitMount2:GameObject;
var FruitMount3:GameObject;
var FruitMount4:GameObject;
var FruitMount5:GameObject;
var FruitMount6:GameObject;
var FruitMount7:GameObject;
var FruitMount8:GameObject;
var FruitMount9:GameObject;
var FruitMount10:GameObject;
var FruitMount11:GameObject;
var FruitMount12:GameObject;
var FruitMount13:GameObject;
var FruitMount14:GameObject;
var FruitMount15:GameObject;

var growthRate:int;
var berryGrower:int;
var berryGrowthTime:int=7;


function Start () {
	GameObject.Find("GameController").SendMessage("addPoints", 5);
	hasFruit=false;
	ageDays=0;
	berryGrower=0;
	InvokeRepeating("nextDay", 0, 3);


}

function nextDay(){
	grow();
}

function grow(){
	//add days
	//
		ageDays++;
		if (hasFruit==false){
			berryGrower++;
		}
}

function harvest(){
	for (var child : Transform in transform) {
	    		if (child.GetComponent(Berry)){
	    			Debug.Log("picked "+child);
					//child.GetComponent(Rigidbody).useGravity=true;
					//child.GetComponent(Rigidbody).isKinematic=false;
					child.SendMessage("pick");
				}
	    	}
	    	hasFruit=false;
}

function knock(){
	dropBerries();
}

function dropBerries(){
	Debug.Log("Drop berries");
	for (var child : Transform in transform) {
    		Debug.Log("childs: "+child);
    		if (child.GetComponent(Berry)){
				//child.GetComponent(Rigidbody).useGravity=true;
				//child.GetComponent(Rigidbody).isKinematic=false;
				child.SendMessage("drop");
			}
    	}
}
function growBerries(){
		var pos;
		
		pos=FruitMount1.transform.position;
		var myFruit1 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit1.transform.parent=transform;
		myFruit1.GetComponent(Berry).growthRate=growthRate;
		
		pos=FruitMount2.transform.position;
		var myFruit2 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit2.transform.parent=transform;
		myFruit2.GetComponent(Berry).growthRate=growthRate;
		
		pos=FruitMount3.transform.position;
		var myFruit3 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit3.transform.parent=transform;
		myFruit3.GetComponent(Berry).growthRate=growthRate;
		
		pos=FruitMount4.transform.position;
		var myFruit4 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit4.transform.parent=transform;
		myFruit4.GetComponent(Berry).growthRate=growthRate;
		
		pos=FruitMount5.transform.position;
		var myFruit5 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit5.transform.parent=transform;
		myFruit5.GetComponent(Berry).growthRate=growthRate;
		
		pos=FruitMount6.transform.position;
		var myFruit6 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit6.transform.parent=transform;
		myFruit6.GetComponent(Berry).growthRate=growthRate;
		
		pos=FruitMount7.transform.position;
		var myFruit7 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit7.transform.parent=transform;
		myFruit7.GetComponent(Berry).growthRate=growthRate;
		
		pos=FruitMount8.transform.position;
		var myFruit8 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit8.transform.parent=transform;
		myFruit8.GetComponent(Berry).growthRate=growthRate;
		
		pos=FruitMount9.transform.position;
		var myFruit9 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit9.transform.parent=transform;
		myFruit9.GetComponent(Berry).growthRate=growthRate;
		
		pos=FruitMount10.transform.position;
		var myFruit10 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit10.transform.parent=transform;
		myFruit10.GetComponent(Berry).growthRate=growthRate;
		
		pos=FruitMount11.transform.position;
		var myFruit11 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit11.transform.parent=transform;
		myFruit11.GetComponent(Berry).growthRate=growthRate;
		
		pos=FruitMount12.transform.position;
		var myFruit12 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit12.transform.parent=transform;
		myFruit12.GetComponent(Berry).growthRate=growthRate;
		
		pos=FruitMount13.transform.position;
		var myFruit13 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit13.transform.parent=transform;
		myFruit13.GetComponent(Berry).growthRate=growthRate;
		
		pos=FruitMount14.transform.position;
		var myFruit14 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit14.transform.parent=transform;
		myFruit14.GetComponent(Berry).growthRate=growthRate;
		
		pos=FruitMount15.transform.position;
		var myFruit15 : GameObject = Instantiate(myFruit, pos, transform.rotation);
		myFruit15.transform.parent=transform;
		myFruit15.GetComponent(Berry).growthRate=growthRate;
		
		hasFruit=true;

}

function mulch(){
	Debug.Log("mulch");
	var pos;
	pos = transform.position;
	for (var m = 0;m<=growthRate;m++){
		Instantiate(Resources.Load("mulchBit", GameObject), pos, transform.rotation);
		Debug.Log("mulch "+m);
	}
	Destroy(this.gameObject);
}

function Update () {

	if (berryGrower>=berryGrowthTime){
		hasFruit=true;
		berryGrower=0;
		growBerries();
	}

}