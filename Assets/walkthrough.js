﻿#pragma strict

var step:int;

function Start () {
	step=1;
}

function Update () {

	if(step==1){
		if (Input.GetKeyDown(KeyCode.Space)){
			guiText.text="This is your Seeds Menu. It holds all the seeds you can plant. \nClick on the picture of a plant to grab its seed.";
			step=2;
		}
	}
	
	if (step==2){
		if(Input.GetMouseButtonDown(0)){
			guiText.text="Using the mouse, move the seed where you'd like it to go. Drop it by pressing the space bar.";
			step=3;
		}
	}
	
	if (step==3){
		if(Input.GetKeyDown(KeyCode.Space)){
			guiText.text="You planted your first seed! Wait for it to grow and then click on your plant.";
			step=4;
		}
	}
	
	if (step==4){
		if(Input.GetMouseButtonDown(0)){
			guiText.text="Clicking on plants selects them.\n When a plant is selected you can see information about it.  \n Wait until your plant has fruit and press the [P] key while it's selected.";
			step=5;
		}
	}
	
	if (step==5){
		if(Input.GetKeyDown(KeyCode.P)){
			guiText.text="You picked some fruit! \nLook at how many points you've earned! \nNow keep gardening...";
			step=6;
		}
	}
	
	if (step==6){
		if(Input.GetMouseButtonDown(0)){
			guiText.text="Have fun and don't be afraid to experiment. In this demo you'll never run out of seeds!\n Explore the garden by moving your mouse to the edges of the screen.";
			step=7;
		}
	}
	
	if (step==7){
		if(Input.GetKeyDown(KeyCode.Space)){
			guiText.text="";
			step=8;
		}
	}
	
	

}


//if left click
//if(Input.GetMouseButtonDown(0)){
//if space
//if (Input.GetKeyDown(KeyCode.Space)){