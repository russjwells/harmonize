﻿#pragma strict
var growthRate : int;
var daysDropped : int;
var ageDays : int;
var dropped : boolean;


function Start () {
	ageDays=0;
	InvokeRepeating("nextDay", 0, 3);
	//yeild WaitForSeconds(.25);
	rigidbody.useGravity=false;
	rigidbody.isKinematic=true;
	dropped=false;
}

function Update () {
	if (daysDropped>20){
		var chance:int = Random.Range(1, 101);
		if (chance>=50){
			var newPlant : GameObject = Instantiate(Resources.Load("Goggi Seed", GameObject), transform.position, transform.rotation);
			Destroy(this.gameObject);
		}else{
			Destroy(this.gameObject);
		}
	}
	if (ageDays>50&&dropped==false){
		drop();
	}
}

function nextDay(){
	grow();
}

function grow(){
	//add days
	//
		ageDays++;
		if (dropped){
			daysDropped++;
		}
	}

function pick(){
	Debug.Log("berry picked");
	var points:int=0;
	//*growthRate
	points = points + (growthRate);
	//Debug.Log(points);
	GameObject.Find("GameController").SendMessage("addPoints", points);
	//GameObject.Find("GameController").GetComponent(gameController).addPoints(points);
	CancelInvoke("nextDay");
	GameObject.Destroy(gameObject);
	
}

function drop(){
	Debug.Log("dropped");
	dropped=true;
	transform.parent=null;
	rigidbody.useGravity=true;
	rigidbody.isKinematic=false;
	
	

}