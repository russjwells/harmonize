﻿#pragma strict

var ageDays:int;
var growthRate:int;

function Start () {
	//GetComponent(MeshCollider).enabled=false;
	InvokeRepeating("nextDay", 0, 3);
	ageDays=0;
	growthRate=Random.Range(1, 101);

}

function Update () {

}

function nextDay(){
	//ageDays++;
	grow();
}

function grow(){
	ageDays = ageDays + 1;
	
	if (ageDays>1){
	
	}
	
	if (ageDays >2){
		makeBushes();
	}else{
		
	}
}

function makeBushes(){
	//create the little tree
	var newGoggiBerryBushes : GameObject = Instantiate(Resources.Load("goggiBerryBushes", GameObject));
	//place the tree
	newGoggiBerryBushes.transform.position=transform.position;
	newGoggiBerryBushes.transform.position.y=transform.position.y+.25;
	//random rotation
	var randomRotation = Quaternion.Euler( 0 , Random.Range(0, 360) , 0);
	newGoggiBerryBushes.transform.rotation=randomRotation;
	//transfer age and growth rate
	newGoggiBerryBushes.GetComponent(goggiBerryBushes).ageDays=ageDays;
	newGoggiBerryBushes.GetComponent(goggiBerryBushes).growthRate=growthRate;
	//transfer selection
	if (gameObject.GetComponent(selectable).isSelected==true){
		GameObject.Find("GameController").SendMessage("select", newGoggiBerryBushes);
	}
	//update info
	//destroy seed
	GameObject.Find("GameController").SendMessage("addPoints",5);
	Destroy(gameObject);
	
}